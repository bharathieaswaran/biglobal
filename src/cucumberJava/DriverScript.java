
package cucumberJava;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ios.IOSDriver;

import com.cucumber.listener.ExtentCucumberFormatter;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
//import org.junit.AfterClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.bson.types.ObjectId;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.cucumber.listener.ExtentProperties;
import com.cucumber.listener.Reporter;

//@CucumberOptions(plugin = {"json:output/cucumber.json"}, glue = { "steps" }, features = { "FeatureFiles" })
@CucumberOptions(plugin = {"com.cucumber.listener.ExtentCucumberFormatter:","json:target/cucumber.json"}, glue = { "steps" }, features = { "FeatureFiles" })
public class DriverScript extends AbstractTestNGCucumberTests {
	public static String deviceName;
	public static DesiredCapabilities capabilities;
	public static AppiumDriver driver;

	@BeforeClass
	@Parameters({"device"})
	public static void setup(String Device) throws ConfigurationException, MalformedURLException {
		PropertiesConfiguration config = new PropertiesConfiguration("config.properties");
		String userDir = System.getProperty("user.dir");	
		ExtentProperties extentProperties = ExtentProperties.INSTANCE;
		Calendar cal = Calendar.getInstance();	
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		String Timestamp =dateFormat.format(cal.getTime());
		deviceName =Device;
		extentProperties.setReportPath("target/"+Timestamp+"/SHreport.html");
		DesiredCapabilities capabilities = new DesiredCapabilities("", "", Platform.ANY);
		capabilities.setCapability("model",Device);
		capabilities.setCapability("autoLaunch", true);
		capabilities.setCapability("bundleId", "Global-BI-iPhone-Dev.vpn");
		capabilities.setCapability("autoLaunch", true);
		String host = "adidas.perfectomobile.com";
		capabilities.setCapability("user", "bharathi.easwaran@externals.adidas-group.com");
		capabilities.setCapability("password", "Adidas$2017");
		capabilities.setCapability("scriptName", "BIAutomation");
		//capabilities.setCapability("automationName", "XCUITest");
		//capabilities.setCapability("automationName", "Appium");
		driver = new IOSDriver(new URL("https://" + host + "/nexperience/perfectomobile/wd/hub"), capabilities);
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);	
	}

	@AfterClass
	public static void teardown() {
	
	}
}
