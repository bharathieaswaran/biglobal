@MT-56
Feature: 

#@TEST_MT
 Scenario Outline: Check if Cancel comment works 
 Given I start application by name "stockHERO" and navigate to the starting point
 When I choose the "<country>" from the country picker
  Then I should see the list of stores
  	When I select a shop with "<StoreID>"
  	Then map should be displayed with "<StoreName>" and "<StoreAddress>"
  	When I choose "Join" option to log in
  	Then I log in to app as role "<LoginRole>" using "<MyName>"
    When I navigate to the "Search" button
    And  I navigate to search box and look for shoes' model "C77124"
    When I am on the chosen shoe site I select shoe size "5"
    And I click on "Comment" button and enter comment
    Then I cancel the comment
   And I close my application by name "stockHERO"
 
 Examples: 
  |StoreID	|country|city		|StoreName										  |StoreAddress							|MyName		  | LoginRole  |
   |3425	 	|Spain  |Zaragoza 	|adidas Shop-in-Shop Zaragoza, Paseo Independencia|Paseo Independencia,11,50001,Zaragoza|Test-1stFloor| SalesFloor |
   
	@TEST_MT-45
	Scenario Outline: Change Role and Login
		  Given I start application by name "stockHERO" and navigate to the starting point
		  When I choose the "<country>" from the country picker
		  Then I should see the list of stores
		  When I select a shop with "<StoreID>"
		  Then map should be displayed with "<StoreName>" and "<StoreAddress>"
		  When I choose "Join" option to log in
		  Then I log in to app as role "<LoginRole>" using "<MyName>"
		  Then I navigate to drop down menu button
		  Then I enter "<ChangeRole>" into "Role" option
		 	Then Logout successfully
		  And I close my application by name "stockHERO"
		  
		  Examples: 
		    |StoreID	|country|city		|StoreName										  |StoreAddress							|MyName		  | LoginRole  | ChangeRole |
		    |3425	 	|Spain  |Zaragoza 	|adidas Shop-in-Shop Zaragoza, Paseo Independencia|Paseo Independencia,11,50001,Zaragoza|Test-1stFloor| Sales Floor | Stock Room|
		  	
	@TEST_MT-47
	Scenario Outline: Send the app to background
		 		Given I start application by name "stockHERO" and navigate to the starting point
		 		When I choose the "<country>" from the country picker
		  	Then I should see the list of stores
		  	When I select a shop with "<StoreID>"
		  	Then map should be displayed with "<StoreName>" and "<StoreAddress>"
		  	When I choose "Join" option to log in
		  	Then I log in to app as role "<LoginRole>" using "<MyName>"
		    When I navigate to the "Search" button
		    And  I navigate to search box and look for shoes' model "C77124"
		    When I push the application to the background without logging off
		    Then I reopen the application "stockHERO"
		    Then Logout successfully
		    And I close my application by name "stockHERO"
		    
		 Examples: 
		    |StoreID	|country|city		|StoreName										  |StoreAddress							|MyName		  | LoginRole  |
		    |3425	 	|Spain  |Zaragoza 	|adidas Shop-in-Shop Zaragoza, Paseo Independencia|Paseo Independencia,11,50001,Zaragoza|Test-1stFloor| Sales Floor |  	

	@TEST_MT-44
	Scenario Outline: Search an Invalid store from the store screen
		    Given I start application by name "stockHERO" and navigate to the starting point
		    When I choose the "<country>" from the country picker
		    Then I should see the list of stores
		   	When I search for a store using an "<InvalidStoreID>" via the search bar
		    Then No search result found should be displayed 
		    And I close my application by name "stockHERO"
		
		  Examples: 
		
		    |InvalidStoreID	|country|city	 |StoreName									       |StoreAddress						    |MyName|
		    |9999			|Spain	|Zaragoza|adidas Shop-in-Shop Zaragoza, Paseo Independencia|Paseo Independencia,11,50001,Zaragoza   |Test-1stFloor|	

	
	@TEST_MT-42
	Scenario Outline: Search a Valid store from the store screen
			Given I start application by name "stockHERO" and navigate to the starting point
		    When I choose the "<country>" from the country picker
		    Then I should see the list of stores
		    When I select a shop with "<StoreID>"
		    Then map should be displayed with "<StoreName>" and "<StoreAddress>"
		    When I choose "Join" option to log in
		    Then I log in to app as role "<LoginRole>" using "<MyName>"
		    Then Logout successfully
			  And I close my application by name "stockHERO"
		
		    Examples: 
		      |StoreID|country|city		|StoreName											|StoreAddress						  | MyName		|
		      |3425   |Spain  |Zaragoza |adidas Shop-in-Shop Zaragoza, Paseo Independencia	|Paseo Independencia,11,50001,Zaragoza|Test-1stFloor|	

	@TEST_MT-52
	Scenario Outline:  Change Default size Settings
		  Given I start application by name "stockHERO" and navigate to the starting point
		  When I choose the "<country>" from the country picker
		  Then I should see the list of stores
		  When I select a shop with "<StoreID>"
		  Then map should be displayed with "<StoreName>" and "<StoreAddress>"
		  When I choose "Join" option to log in
		 Then I log in to app as role "<LoginRole>" using "<MyName>"
		  Then I navigate to drop down menu button
		  Then I enter "<SizeUnit>" into "Default Size" option
		  #Then I navigate to drop down menu button
		  Then Logout successfully
		  And I close my application by name "stockHERO"
		
		 Examples: 
		    |StoreID	|country|city		|StoreName										  |StoreAddress							|MyName		    | LoginRole  | SizeUnit |
		    |3425	 	|Spain  |Zaragoza 	|adidas Shop-in-Shop Zaragoza, Paseo Independencia|Paseo Independencia,11,50001,Zaragoza|Test-1stFloor  |StockRoom   |UK |
		 	
	@TEST_MT-50
	Scenario Outline: Click Cancel button instead of Joining the store
		    When I start application by name "stockHERO" and navigate to the starting point
		    When I choose the "<country>" from the country picker
		    Then I should see the list of stores
		    When I select a shop with "<StoreID>"
		    Then map should be displayed with "<StoreName>" and "<StoreAddress>"
		    And I choose "Join" option to log in
		    Then I enter "<MyName>" to the name box and click on Cancel button
		    And I close my application by name "stockHERO"
		    
		    Examples: 
		    |StoreID	|country|city		|StoreName										  |StoreAddress							|MyName		  | LoginRole  | ChangeRole |
		    |3425	 	|Spain  |Zaragoza 	|adidas Shop-in-Shop Zaragoza, Paseo Independencia|Paseo Independencia,11,50001,Zaragoza|Test-1stFloor| Sales Floor | StockRoom  |
			
	@TEST_MT-43
	Scenario Outline: Join the store as Stock Room employee and request Superstar shoes
				    Given I start application by name "stockHERO" and navigate to the starting point
				    When I choose the "<country>" from the country picker
				    Then I should see the list of stores
				    When I select a shop with "<StoreID>"
				    Then map should be displayed with "<StoreName>" and "<StoreAddress>"
				    When I choose "Join" option to log in
				    Then I log in to app as role "<LoginRole>" using "<MyName>"
				    Then I navigate to search box and look for shoes' model "C77124"   
				  	When I am on the chosen shoe site I select shoe size "6"
				  	And I request chosen article
				  	Then Task status should be "open" on the screen 
				  	Then I should see requested article in Tasks screen and press Grab button
				  	Then Task status should be "inprogress" on the screen
				  	Then I should see requested article in Tasks screen and press "confirm" button
				  	Then Task status should be "confirmed" on the screen
				  	Then Logout successfully
				    And I close my application by name "stockHERO"
				   
				   Examples: 
				    |country|StoreID|city	 |StoreName											|StoreAddress						  |MyName		 |  LoginRole|
				    |Spain  |3425  	|Zaragoza|adidas Shop-in-Shop Zaragoza, Paseo Independencia |Paseo Independencia,11,50001,Zaragoza|Test-2stFloor$| Sales Floor|	

	
	@TEST_MT-49
	Scenario Outline: Join the store as Stock Room employee and request Superstar shoes and validate decline
		    Given I start application by name "stockHERO" and navigate to the starting point
		    When I choose the "<country>" from the country picker
		    Then I should see the list of stores
		    When I select a shop with "<StoreID>"
		    When I choose "Join" option to log in
		  	Then I log in to app as role "<LoginRole>" using "<MyName>"
		    Then I navigate to search box and look for shoes' model "C77124"   
		  	When I am on the chosen shoe site I select shoe size "6"
		  	And I request chosen article
		  	Then Task status should be "open" on the screen
		  	Then I should see requested article in Tasks screen and press Grab button
		  	Then Task status should be "inprogress" on the screen
		  	Then I should see requested article in Tasks screen and press "decline" button
		  	Then Task status should be "declined" on the screen
		  	Then Logout successfully
		 	  And I close my application by name "stockHERO"
		   Examples: 
		    |country|StoreID|city	 |StoreName											|StoreAddress						  |MyName		 | LoginRole|
		    |Spain  |3425  	|Zaragoza|adidas Shop-in-Shop Zaragoza, Paseo Independencia |Paseo Independencia,11,50001,Zaragoza|Test-1stFloor$| Sales Floor|	

	@TEST_MT-46
	Scenario Outline: Select a Filter and apply
				    Given I start application by name "stockHERO" and navigate to the starting point
				 	  When I choose the "<country>" from the country picker
				 	  Then I should see the list of stores
				  	When I select a shop with "<StoreID>"
				  	When I choose "Join" option to log in
				  	Then I log in to app as role "<LoginRole>" using "<MyName>"
				   	Then I navigate to search box and look for shoes' model "S80682"   
				  	When I am on the chosen shoe site I select shoe size "6"
				  	And I request chosen article
				  	Then I navigate to search box and look for shoes' model "C77124"   
				  	When I am on the chosen shoe site I select shoe size "6"
				  	And I request chosen article
				  	When I navigate to "Filter" button
				  	And I see the filter-menu option "Women"
					  Then I apply "Women" filter
				   	When I click on "Apply" button
				   	Then I should see article "S80682" on the screen
				   	Then Logout successfully
				    And I close my application by name "stockHERO"
				  Examples: 
		    |country|StoreID|city	 |StoreName											|StoreAddress						  |MyName		 | LoginRole|
		    |Spain  |3425  	|Zaragoza|adidas Shop-in-Shop Zaragoza, Paseo Independencia |Paseo Independencia,11,50001,Zaragoza|Test-1stFloor$| Sales Floor|