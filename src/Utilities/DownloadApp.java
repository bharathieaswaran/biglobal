package Utilities;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ios.IOSDriver;


public class DownloadApp {
	@Test
	public void SAMapp() throws Exception{
		IOSDriver driver;
		DesiredCapabilities capabilities =setupCapabilities();
		String host = "adidas.perfectomobile.com";
		driver = new IOSDriver(new URL("https://" + host + "/nexperience/perfectomobile/wd/hub"), capabilities);

		//driver=new  IOSDriver(new URL("http://127.0.0.1:4723/wd/hub"),capabilities);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.id("search_action")).click();
		driver.findElement(By.id("search_input")).clear();
		driver.findElement(By.id("search_input")).sendKeys("Global BI Test");
		try {
		if (driver.findElement(By.xpath("//*[@class='catalog_item_list search_results']//*[@class='install dark_blue']")).isDisplayed()){
			driver.findElement(By.xpath("//*[@class='catalog_item_list search_results']//*[@class='install dark_blue']")).click();	
		}
		if (driver.findElement(By.xpath("//*[@class='catalog_item_list search_results']//*[@class='install dark']")).isDisplayed()){
			driver.findElement(By.xpath("//*[@class='catalog_item_list search_results']//*[@class='install dark']")).click();	
		}
		if (driver.findElement(By.xpath("//*[@text='Processing']")).isDisplayed()){
			driver.findElement(By.xpath("//*[@text='Processing']")).click();	
		}
		Thread.sleep(2000);
		if (driver.findElement(By.xpath("//*[text()='  Update ']")).isDisplayed()){
			driver.findElement(By.xpath("//*[text()='  Update ']")).click();	
		}
		}catch(Exception e){}
		
		//driver.findElement(By.xpath("//*[text()='  Update ']")).click();
		Thread.sleep(60000);
		driver.close();
		driver.quit();	
    }

	public DesiredCapabilities setupCapabilities() throws IOException {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		
		capabilities.setCapability("model","iPhone-6");
		capabilities.setCapability("user", "bharathi.easwaran@externals.adidas-group.com");
		capabilities.setCapability("password", "Adidas$2017");

		capabilities.setCapability(CapabilityType.BROWSER_NAME, "safari");
		return capabilities;
		
	}

}