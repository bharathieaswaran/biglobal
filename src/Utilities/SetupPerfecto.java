package Utilities;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Parameters;

import com.perfecto.reportium.client.ReportiumClient;
import com.perfecto.reportium.client.ReportiumClientFactory;
import com.perfecto.reportium.model.PerfectoExecutionContext;
import com.perfecto.reportium.model.Project;
import com.perfecto.reportium.test.result.TestResultFactory;
import ObjectRepo.IOSElementsList;
import cucumberJava.DriverScript;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ios.IOSDriver;

public class SetupPerfecto {
	public DesiredCapabilities capabilities = null;
	public AppiumDriver driver;

		@Parameters({"device"})
		public DesiredCapabilities setupCapabilities(String device) throws IOException {
		DesiredCapabilities capabilities = new DesiredCapabilities("", "", Platform.ANY);
		//capabilities.setCapability("model", "iPhone-7");
		capabilities.setCapability("model",device);
		capabilities.setCapability("autoLaunch", true);
		System.out.println("device Name = "+DriverScript.deviceName );
		//capabilities.setCapability("automationName", "XCUITest");
		//capabilities.setCapability("bundleId", "Global-BI-iPhone-Test.vpn");
		//capabilities.setCapability("bundleId", "Global-BI-iPhone-Dev.vpn");
		capabilities.setCapability("autoLaunch", true);
		String host = "adidas.perfectomobile.com";
		capabilities.setCapability("user", "bharathi.easwaran@externals.adidas-group.com");
		capabilities.setCapability("password", "Adidas@1234");
		capabilities.setCapability("scriptName", "BIAutomation");
		return capabilities;
	}

	
		public DesiredCapabilities setupCapabilities1() throws IOException {
		// DesiredCapabilities capabilities = new
		// DesiredCapabilities(browserName, "", Platform.ANY);
		DesiredCapabilities capabilities = new DesiredCapabilities("", "", Platform.ANY);
		//capabilities.setCapability("model", "iPad Air 2");
		capabilities.setCapability("model", "A1538");
		capabilities.setCapability("version", "11");
		capabilities.setCapability("deviceName", "iPad");
		capabilities.setCapability("deviceName", "iPad");
		capabilities.setCapability("automationName", "XCUITest");
		capabilities.setCapability("app", "com.adidas.runner");
		capabilities.setCapability("autoLaunch", true);
		//capabilities.setCapability("bundleId", "com.adidas.runner");
		// capabilities.setCapability("autoLaunch", true);
		String host = "";
		// IOSDriver driver = new IOSDriver(new URL("https://" + host +
		// "/nexperience/perfectomobile/wd/hub"), capabilities);
		// AppiumDriver<?> driver = new IOSDriver(new URL("https://" + host +
		// "/nexperience/perfectomobile/wd/hub"), capabilities);
		return capabilities;
	}
	public ReportiumClient Report(WebDriver driver) {
		ReportiumClient reportiumClient = new ReportiumClientFactory()
				.createPerfectoReportiumClient(new PerfectoExecutionContext.PerfectoExecutionContextBuilder()
						.withProject(new Project("Sample Selenium-Reportium", "1.0")).withContextTags("Regression") // Optional
						.withWebDriver(driver) // Optional
						.build());
		return reportiumClient;
	}

	public void ClickonElement(WebDriver driver, By by) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(by));
		element.click();
	}

	public void VerifyElementDisplayed(ReportiumClient Report, WebDriver driver, By by) {

		try {
			WebDriverWait wait = new WebDriverWait(driver, 60);
			WebElement element = wait.until(ExpectedConditions.elementToBeClickable(by));
		} catch (Throwable t) {
		}
	}

	private static Map<String, String> getAppParams(String app, String by) {
		Map<String, String> params = new HashMap<String, String>();
		params.put(by, app);
		return params;
	}

	public void closeApp(String app, String by) {
		driver.executeScript("mobile:application:close", getAppParams(app, by));
	}

	public void scroll(AppiumDriver driver, Double startY, Double endY) {
		Dimension dimensions = driver.manage().window().getSize();
		Double screenHeightStart = dimensions.getHeight() * startY;
		int scrollStart = screenHeightStart.intValue();
		Double screenHeightEnd = dimensions.getHeight() * endY;
		int scrollEnd = screenHeightEnd.intValue();
		driver.swipe(5, scrollStart, 5, scrollEnd, 50);
	}

	public void findElementWithScroll(AppiumDriver driver, By by) {
		int count = 0;
		while (!isElementVisible(driver, by) && count < 10) {
			scroll(driver, 0.7, 0.2);
			count++;
		}
	}

	public Boolean isElementVisible(AppiumDriver driver, By by) {
		try {
			return driver.findElement(by).isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}



	
}
