/**
 * 
 */
package Utilities;

import ObjectRepo.IOSElementsList;
import Utilities.IOSOperations;
import Utilities.SetupPerfecto;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberJava.DriverScript;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ios.IOSDriver;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.junit.Assert;

public class LoginStepsDefs {
	AppiumDriver driver;
	DesiredCapabilities capabilities;
	IOSOperations iosOpn;

	   
	    public void before(String device) throws IOException {
			String host = "adidas.perfectomobile.com";
			SetupPerfecto perfecto = new SetupPerfecto();
			capabilities = perfecto.setupCapabilities(DriverScript.deviceName);
			capabilities.setCapability("model", DriverScript.deviceName);
			driver = new IOSDriver(new URL("https://" + host + "/nexperience/perfectomobile/wd/hub"), capabilities);
			iosOpn = new IOSOperations();
			
	    }
	    @After
	    public void after() {
	        driver.quit();
	    }

	public AppiumDriver getdriver() {
		return this.driver;
	}
/*
	@Given("^I start application by name \"(.*?)\" and navigate to the starting point$")
	public void startAppByNameWithStartingPoint(String name) throws InterruptedException, IOException {
		driver = getdriver();
		String bodyText;
		WebDriverWait wait;
		iosOpn.clickWhileElementExist(driver, IOSElementsList.btnBackNavigationBar);
	
		if (iosOpn.isElementExist(driver, IOSElementsList.btnArticlesNavigationBar)) {
			if (!iosOpn.isElementExist(driver, IOSElementsList.btnHelp)) {
				iosOpn.clickOnElement(driver, IOSElementsList.btnMenu);
			}
			iosOpn.clickOnElement(driver, IOSElementsList.btnLogout);
		}
		if (iosOpn.isElementExist(driver, IOSElementsList.btnTasksNavigationBar)) {
			if (!iosOpn.isElementExist(driver, IOSElementsList.btnHelp)) {
				iosOpn.clickOnElement(driver, IOSElementsList.btnMenu);
			}
			iosOpn.clickOnElement(driver, IOSElementsList.btnLogout);
		}
		iosOpn.clickWhileElementExist(driver, IOSElementsList.btnBackNavigationBar);
	}

	@Then("^I reopen the application \"(.*?)\"$")
	public void reopenApp(String appName) {
		iosOpn.validateElementExist(driver, IOSElementsList.btnArticlesNavigationBar);
		iosOpn.clickOnElement(driver, IOSElementsList.btnArticlesScreenBack);
	}

	@Then("^I cancel the comment$")
	public void cancelClk() {
		iosOpn.clickOnElement(driver, IOSElementsList.btnCancel);
	}

	@When("^I click on \"(.*?)\" button and enter comment$")
	public void cancelCmt(String Comment) {

		Map<String, Object> params1 = new HashMap<>();
		iosOpn.isElementExist(driver, IOSElementsList.btnArticlesNavigationBar);
		Map<String, Object> params2 = new HashMap<>();
        params2.put("label", "Request");
        params2.put("threshold", "70");
        params2.put("operation", "single");
        params2.put("ignorecase", "nocase");
        params2.put("label.direction", "right");
        params2.put("label.offset", "25%");
        Object result1 = driver.executeScript("mobile:button-text:click", params2);  
		driver.findElementByXPath("//UIATextView").sendKeys("Order More Later");
			
	}

	// I choose the '<country>' from the country picker
	@When("^I choose the \"(.*?)\" from the country picker$")
	public void chooseCountryFromList(String country) throws InterruptedException {
		By by = By.xpath("//*[@label='" + country + "']");
		iosOpn.findElementWithScroll(driver, by);
		iosOpn.clickOnElement(driver, by);
		if (iosOpn.isElementExist(driver, IOSElementsList.btnSelectCountryNavigationBar)) {
			iosOpn.clickOnElement(driver, by);
		}
	}

	public Boolean isElementVisible1(AppiumDriver driver, By by) {
		try {
			return driver.findElement(by).isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	@When("^I search for a store using an \"(.*?)\" via the search bar$")
	public void chooseInvalidStoreID(String InvalidStoreID) {
		System.out.println(InvalidStoreID);
		iosOpn.sendKeysOnElement(driver, IOSElementsList.txtArticleSearch, InvalidStoreID);
		iosOpn.clickOnElement(driver, IOSElementsList.btnArticleSearch);
	}

	@When("^I push the application to the background without logging off$")
	public void sendAppBackground() {
		driver.runAppInBackground(10);
	}

	@And("^I close my application by name \"(.*?)\"$")
	public void closeApp(String name) {
		System.out.println("Close App by name");
		driver.closeApp();
	}

	@And("^I navigate to \"(.*?)\" option$")
	public void reminder(String setReminder) {
		iosOpn.clearElement(driver, IOSElementsList.btnReminder);
	}

	@Then("^I enter \"(.*?)\" into \"(.*?)\" option$")
	public void reminderUnit(String changeRole, String Rnd) {
		driver.findElementByXPath("//*[@label='" + Rnd + "']").click();
		iosOpn.sendKeysOnElement(driver, IOSElementsList.pickerWheel, "Stock Room");
		driver.findElementByXPath("//*[@label='" + Rnd + "']").click();
	}

	@Then("^I should see the list of stores$")
	public void seeListOfStores() {
		iosOpn.validateElementExist(driver, IOSElementsList.tblSearchResults);
	}

	@When("^No search result found should be displayed$")
	public void noSearchResultFound() throws Throwable {
		iosOpn.validateElementExist(driver, IOSElementsList.tblEmptyList);
	}

	@When("^I navigate to the \"(.*?)\" button$")
	public void searchBtn(String searchBtn) {	
		driver.findElement(IOSElementsList.btnArticles).click();
	}

	@Then("^I enter \"(.*?)\" to the name box and click on Cancel button")
	public void logIn(String storeID) {
		iosOpn.clickOnElement(driver, IOSElementsList.btnSalesFloor);
		iosOpn.clickOnElement(driver, IOSElementsList.txtUserName);
		iosOpn.sendKeysOnElement(driver, IOSElementsList.txtUserName, "Test-1stFloor");
		iosOpn.clickOnElement(driver, IOSElementsList.btnLoginDone);
		iosOpn.clickOnElement(driver, IOSElementsList.btnLoginCancel);
	}

	@Then("^I navigate to drop down menu button$")
	public void findMenuBtn() {

		// check if the menu button is visible on the screen, if true -> click
		iosOpn.clickOnElement(driver, IOSElementsList.btnMenu);
		if (!iosOpn.isElementExist(driver, IOSElementsList.btnHelp)) {
			iosOpn.clickOnElement(driver, IOSElementsList.btnMenu);
		}
	}

	@Then("^I select a shop with \"(.*?)\"$")
	public void chooseStoreID(String storeID) throws InterruptedException {
		By by = By.xpath("//*[@label='" + storeID.trim() + "']");
		iosOpn.findElementWithScroll(driver, by);
		iosOpn.clickOnElement(driver, by);
		if (iosOpn.isElementExist(driver, IOSElementsList.btnSelectCountryNavigationBar)) {
			iosOpn.clickOnElement(driver, by);
		}
	}

	@Then("^I navigate to search box and look for shoes' model \"(.*?)\"$")
	public void goToSearchBox(String ShoeType) {
		iosOpn.clickOnElement(driver, IOSElementsList.btnArticles);
		iosOpn.clearElement(driver, IOSElementsList.txtSearch);
		iosOpn.sendKeysOnElement(driver, IOSElementsList.txtSearch, ShoeType);
		iosOpn.clickOnElement(driver, IOSElementsList.btnArticleSearch);
		By by = By.xpath("//*[@label='" + ShoeType + "']");
		iosOpn.clickOnElement(driver, by);

	}

	@When("^I am on the chosen shoe site I select shoe size \"(.*?)\"$")
	public void chooseShoeSize(String shoeSize) {
		iosOpn.validateElementExist(driver, IOSElementsList.btnArticles);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		Map<String, Object> shoe = new HashMap<>();
		shoe.put("content", shoeSize);
		shoe.put("timeout", "30");
		shoe.put("words", "words");
		executor.executeScript("mobile:text:select", shoe);
	}

	@Then("^I should see requested article in Tasks screen and press Grab button$")
	public void tasksList() {
		Boolean Elementstatus = false;
		WebElement QAFelement;
		List<WebElement> childElements = driver.findElement(IOSElementsList.articleTable).findElements(IOSElementsList.articleTableCell);
		for (int i = 0; i < childElements.size(); i++) {
			String getAttri = childElements.get(i).getAttribute("name");
			if (getAttri.contains("open") && Elementstatus == false) {
				childElements.get(i).click();
				Elementstatus = true;
				break;
			}
		}
		QAFelement = driver.findElementByXPath("//*[@label='Grab']");
		QAFelement.click();
	}

	@When("^I request chosen article$")
	public void chooseShoeBtn() throws InterruptedException {
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		Map<String, Object> size = new HashMap<>();
		size.put("label", "REQUEST");
		size.put("timeout", "60");
		executor.executeScript("mobile:button-text:click", size);
	}

	@Then("^Logout successfully$")
	public void Logout() {

		if (!iosOpn.isElementExist(driver, IOSElementsList.btnHelp)) {
			iosOpn.clickOnElement(driver, IOSElementsList.btnMenu);
		}
		iosOpn.clickOnElement(driver, IOSElementsList.btnLogout);
		iosOpn.clickWhileElementExist(driver, IOSElementsList.btnBackNavigationBar);

	}

	@Then("^I should see article \"(.*?)\" on the screen$")
	public void labelWomen(String lblWmn) {

		// check if the WOMEN label is listed on the screen
		iosOpn.clickOnElement(driver, IOSElementsList.filterOptionWomen);
		// driver.findElementByXPath("//*[@label=\"Women\"]");
		WebElement QAFElment;
		List<WebElement> childElements = driver.findElement(IOSElementsList.articleTable)
				.findElements(IOSElementsList.articleTableCell);
		for (int i = 0; i < childElements.size(); i++) {
			String getAttri = childElements.get(i).getAttribute("name");
			if (getAttri.contains("inProgress") && getAttri.contains(lblWmn)) {
				childElements.get(i).isDisplayed();
				break;
			}
		}
	}

	@When("^I click on \"(.*?)\" button$")
	public void applyBtn(String applyBtn) {
		iosOpn.clickOnElement(driver, IOSElementsList.filterApply);
	}

	@Then("^I apply \"(.*?)\" filter$")
	public void chooseFilter(String Choose) {
		iosOpn.clickOnElement(driver, IOSElementsList.filterWomen);
	}

	@When("^I navigate to \"(.*?)\" button$")
	public void applyFilter(String Filter) {
		if (!iosOpn.isElementExist(driver, IOSElementsList.btnTasksNavigationBar)) {
			iosOpn.clickOnElement(driver, IOSElementsList.btnTasks);
		}
		if (!iosOpn.isElementExist(driver, IOSElementsList.btnfilter)) {
			iosOpn.clickOnElement(driver, IOSElementsList.btnTasks);
		} 
		iosOpn.clickOnElement(driver, IOSElementsList.btnfilter);
	}

	@And("^I see the filter-menu option \"(.*?)\"$")
	public void applyWomen(String Women) {
		iosOpn.validateElementExist(driver, IOSElementsList.filterOptionWomen);
	}

	@Then("^I log in to app as role \"(.*?)\" using \"(.*?)\"$")
	public void logIntoStockRoom(String Role, String userName) {
		System.out.println(userName);
		System.out.println(Role.toLowerCase());
		switch (Role.toLowerCase()) {
		case "sales floor":
		case "salesfloor":
			iosOpn.clickOnElement(driver, IOSElementsList.btnUserRoleSalesFloor);
			break;
		case "stockroom":
		case "stock room":
			iosOpn.clickOnElement(driver, IOSElementsList.btnUserRoleStockRoom);
			break;
		default:
			iosOpn.clickOnElement(driver, IOSElementsList.btnUserRoleStockRoom);
			break;
		}
		iosOpn.clickOnElement(driver, IOSElementsList.txtUserName);
		iosOpn.sendKeysOnElement(driver, IOSElementsList.txtUserName, userName);
		iosOpn.clickOnElement(driver, IOSElementsList.btnNext);
		iosOpn.clickOnElement(driver, IOSElementsList.btnLogin);
		Map<String, Object> tasks = new HashMap<>();
		tasks.put("content", "Tasks");
		tasks.put("timeout", "20");
		Object result2 = driver.executeScript("mobile:checkpoint:text", tasks);
		System.out.println("You are Logged In, welcome!");

	}

	@Then("^I choose \"(.*?)\" option to log in$")
	public void findJoinBtn(String joinBtn) {
		System.out.println(joinBtn);
		// check if the Join button is listed on the screen
		iosOpn.clickOnElement(driver, IOSElementsList.btnJoin);
	}

	@Then("^map should be displayed with \"(.*?)\" and \"(.*?)\"$")
	// check if StoreName and StoreAddress is listed on the screen
	public void findStoreNameAndAddress(String storeName, String storeAddress) {

		WebElement elementShopName = getShopElement(1);
		WebElement elementShopStreet = getShopElement(2);
		WebElement elementShopHouseNr = getShopElement(3);
		WebElement elementShopZipCode = getShopElement(4);
		WebElement elementShopCity = getShopElement(5);
		// Check values of elements now for validation
		String shopName = elementShopName.getAttribute("name");
		String shopStreet = elementShopStreet.getAttribute("name");
		String shopHouseNr = elementShopHouseNr.getAttribute("name");
		String shopZipCode = elementShopZipCode.getAttribute("name");
		String shopCity = elementShopCity.getAttribute("name");

		String sep = ",";
		String shopAddressComplete = shopStreet + sep + shopHouseNr + sep + shopZipCode + sep + shopCity;

		System.out.println("Store name to be validated: " + storeName);
		System.out.println("Store address to be validated: " + storeAddress);
		System.out.println("Store name read from the app: " + shopName);
		System.out.println("Store address read from the app: " + shopAddressComplete);

		// Validation
		if (storeName.equals(shopName)) {

			System.out.println("Shop name is correct");
		} else {

			System.out.println("ERROR: Shop name is NOT MATCHING.");
		}

		if (storeAddress.equals(shopAddressComplete)) {

			System.out.println("Store address is correct");
		} else {
			System.out.println("ERROR: Store address is NOT MATCHING.");

		}
	}

	private WebElement getShopElement(int index) {
		return driver.findElementByXPath("//UIAApplication/UIAWindow[1]/UIAStaticText[" + index + "]");
	}

	// I should see requested article in Tasks screen and press confirm button
	@Then("^I should see requested article in Tasks screen and press \"(.*?)\" button$")
	public void taskconfirm(String status) {

		Boolean Elementstatus = false;
		WebElement QAFElment;
		List<WebElement> childElements = driver.findElement(IOSElementsList.articleTable).findElements(IOSElementsList.articleTableCell);
		
		for (int i = 0; i < childElements.size(); i++) {
			String getAttri = childElements.get(i).getAttribute("name");
			if (getAttri.contains("inProgress") && Elementstatus == false) {
				childElements.get(i).click();
				Elementstatus = true;
				break;
			}
		}
		switch (status) {
		case "confirm":
			WebElement element2 = driver.findElementByXPath("//*[@label='icon confirm white']");
			element2.click();
			break;
		case "decline":
			element2 = driver.findElementByXPath("//*[@label='icon decline white']");
			element2.click();
			break;
		}
	}

	@Then("^Task status should be \"(.*?)\" on the screen$")
	public void checkstatus(String status) throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 20);
		// Thread.sleep(3000);
		WebElement QAFElment;
		
		List<WebElement> childElements = driver.findElement(IOSElementsList.articleTable).findElements(IOSElementsList.articleTableCell);
		Boolean Elementstatus = false;
		switch (status) {
		case "open":
			for (int i = 0; i < childElements.size(); i++) {
				String getAttri = childElements.get(i).getAttribute("name");
				if (getAttri.contains("open") && Elementstatus == false) {
					childElements.get(i).click();
					Elementstatus = true;
					break;
				}
			}
			QAFElment = driver.findElement(By.xpath("//*[@name='open']"));
			System.out.println(QAFElment.isDisplayed());
			break;
		case "confirmed":
			for (int i = 0; i < childElements.size(); i++) {
				String getAttri = childElements.get(i).getAttribute("name");
				if (getAttri.contains("confirmed") && Elementstatus == false) {
					childElements.get(i).click();
					Elementstatus = true;
					break;
				}
			}
			QAFElment = driver.findElement(By.xpath("//*[@name='confirmed']"));
			System.out.println(QAFElment.isDisplayed());
			break;
		case "inprogress":
			for (int i = 0; i < childElements.size(); i++) {
				String getAttri = childElements.get(i).getAttribute("name");
				if (getAttri.contains("inProgress") && Elementstatus == false) {
					childElements.get(i).click();
					Elementstatus = true;
					break;
				}
			}
			QAFElment = driver.findElement(By.xpath("//*[@name='inProgress']"));
			System.out.println(QAFElment.isDisplayed());
			break;
		case "decline":
			for (int i = 0; i < childElements.size(); i++) {
				String getAttri = childElements.get(i).getAttribute("name");
				if (getAttri.contains("declined") && Elementstatus == false) {
					childElements.get(i).click();
					Elementstatus = true;
					break;
				}
			}
			QAFElment = driver.findElement(By.xpath("//*[@name='declined']"));
			System.out.println(QAFElment.isDisplayed());
			break;

		}
	}
	*/
}
