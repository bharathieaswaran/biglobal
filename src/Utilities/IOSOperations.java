package Utilities;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.listener.Reporter;

import ObjectRepo.IOSElementsList;
import io.appium.java_client.AppiumDriver;

public class IOSOperations {

	/*public void clickOnElement(AppiumDriver driver, By by) {
		WebElement element = null;
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.elementToBeClickable(by));
			} catch (Exception e) {
			System.out.println(e.getMessage());
			}
			element.click();
	}

	public void sendKeysOnElement(AppiumDriver driver, By by, String testData) {

		try {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			WebElement element = wait.until(ExpectedConditions.elementToBeClickable(by));
			element.sendKeys(testData.trim());
			;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void clearElement(AppiumDriver driver, By by) {

		try {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			WebElement element = wait.until(ExpectedConditions.elementToBeClickable(by));
			element.clear();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public Boolean isElementExist(AppiumDriver driver, By by) {

		try {
			WebDriverWait wait = new WebDriverWait(driver, 5);
			WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(by));
			return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public Boolean isElementDisplayed(AppiumDriver driver, By by) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(by));
			return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	public void clickWhileElementExist(AppiumDriver driver, By by) {
		try {
			while (driver.findElement(by).isDisplayed()) {
				driver.findElement(by).click();
			}
		} catch (Exception e) {
		}
	}

	public void validateElementExist(AppiumDriver driver, By by) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(by));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		if (isElementExist(driver, by)) {
			Assert.assertTrue(true);
		} else {
			//driver.quit();
			Assert.assertTrue(false);
		}
	}

	public void retryStockHero() {
	}

	public void findElementWithScroll(AppiumDriver driver, By by) {
		int count = 0;
		while (!isElementVisible(driver, by) && count < 10) {
			scroll(driver, 0.7, 0.2);
			count++;
		}
	}

	public void scroll(AppiumDriver driver, Double startY, Double endY) {
		Dimension dimensions = driver.manage().window().getSize();
		Double screenHeightStart = dimensions.getHeight() * startY;
		int scrollStart = screenHeightStart.intValue();
		Double screenHeightEnd = dimensions.getHeight() * endY;
		int scrollEnd = screenHeightEnd.intValue();
		driver.swipe(5, scrollStart, 5, scrollEnd, 50);
	}

	public Boolean isElementVisible(AppiumDriver driver, By by) {
		try {
			return driver.findElement(by).isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
*/
	public void clickOnElement(AppiumDriver driver, By by) throws ConfigurationException, IOException {
		WebElement element = null;
		try {
			//WebDriverWait wait = new WebDriverWait(driver, 5);
			//element = wait.until(ExpectedConditions.elementToBeClickable(by));
			//element.click();
			driver.findElement(by).click();
			} catch (Exception e) {
				Calendar cal = Calendar.getInstance();	
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
				String imageName =dateFormat.format(cal.getTime());
				TakeScreenShot(driver, imageName);
				PropertiesConfiguration config = new PropertiesConfiguration("config.properties");
				String relPath =(String) config.getProperty("Relpath");
				Reporter.addScreenCaptureFromPath(relPath, "");	
				Reporter.addStepLog(e.getMessage());
				Assert.fail();
			}	
	}
	public void tapOnElement(AppiumDriver driver, By by) throws ConfigurationException, IOException {
		WebElement element = null;
		try {
			WebDriverWait wait = new WebDriverWait(driver, 5);
			element = wait.until(ExpectedConditions.elementToBeClickable(by));
			driver.tap(1, driver.findElement(by).getLocation().getX(), driver.findElement(by).getLocation().getY(), 100);
			} catch (Exception e) {
				Calendar cal = Calendar.getInstance();	
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
				String imageName =dateFormat.format(cal.getTime());
				TakeScreenShot(driver, imageName);
				PropertiesConfiguration config = new PropertiesConfiguration("config.properties");
				String relPath =(String) config.getProperty("Relpath");
				Reporter.addScreenCaptureFromPath(relPath, "");	
				Reporter.addStepLog(e.getMessage());	
				Assert.fail();
			}	
	}

	public void tapOnCoOrdinates(AppiumDriver driver,int xAxis, int yAxis) {
		WebElement element = null;
		driver.tap(1, xAxis, yAxis, 100);
	}
	public void sendKeysOnElement(AppiumDriver driver, By by, String testData) throws ConfigurationException, IOException {

		try {
			WebDriverWait wait = new WebDriverWait(driver, 5);
			WebElement element = wait.until(ExpectedConditions.elementToBeClickable(by));
			element.sendKeys(testData.trim());
			
		} catch (Exception e) {
			Calendar cal = Calendar.getInstance();	
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
			String imageName =dateFormat.format(cal.getTime());
			TakeScreenShot(driver, imageName);
			PropertiesConfiguration config = new PropertiesConfiguration("config.properties");
			String relPath =(String) config.getProperty("Relpath");
			Reporter.addScreenCaptureFromPath(relPath, "");	
			Reporter.addStepLog(e.getMessage());	
			Assert.fail();
		}
	}

	public void clearElement(AppiumDriver driver, By by) throws ConfigurationException, IOException {

		try {
			WebDriverWait wait = new WebDriverWait(driver, 5);
			WebElement element = wait.until(ExpectedConditions.elementToBeClickable(by));
			element.clear();
		} catch (Exception e) {
			Calendar cal = Calendar.getInstance();	
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
			String imageName =dateFormat.format(cal.getTime());
			TakeScreenShot(driver, imageName);
			PropertiesConfiguration config = new PropertiesConfiguration("config.properties");
			String relPath =(String) config.getProperty("Relpath");
			Reporter.addScreenCaptureFromPath(relPath, "");	
			Reporter.addStepLog(e.getMessage());	
			Assert.fail();
		}
	}

	public Boolean isElementExist(WebDriver driver, By by) {

		try {
			WebDriverWait wait = new WebDriverWait(driver, 3);
			WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(by));
			//return element.isDisplayed();
			return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public String getCssValue(WebDriver driver, By by, String key) {
			WebDriverWait wait = new WebDriverWait(driver, 1);
			WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(by));
			return element.getCssValue(key);
			//background-color
	}
	
	public void staticWait() throws InterruptedException{
		Thread.sleep(20000);
	}
	public Boolean isElementDisplayed(AppiumDriver driver, By by) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 2);
			WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(by));
			return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	public void clickWhileElementExist(AppiumDriver driver, By by) throws ConfigurationException, IOException {
		try {
			while (isElementVisible(driver,by)) {
				driver.findElement(by).click();
			}
		} catch (Exception e) {
			Calendar cal = Calendar.getInstance();	
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
			String imageName =dateFormat.format(cal.getTime());
			TakeScreenShot(driver, imageName);
			PropertiesConfiguration config = new PropertiesConfiguration("config.properties");
			String relPath =(String) config.getProperty("Relpath");
			Reporter.addScreenCaptureFromPath(relPath, "");	
			Reporter.addStepLog(e.getMessage());	
			Assert.fail();
		}
	}

	public void validateElementExist(AppiumDriver driver, By by) throws IOException, ConfigurationException {

		if (isElementExist(driver, by)) {
			Assert.assertTrue(true);
		} else {
			//driver.quit();
			Calendar cal = Calendar.getInstance();	
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
			String imageName =dateFormat.format(cal.getTime());
			TakeScreenShot(driver, imageName);
			PropertiesConfiguration config = new PropertiesConfiguration("config.properties");
			String relPath =(String) config.getProperty("Relpath");
			Reporter.addScreenCaptureFromPath(relPath, "");
			Assert.assertTrue(false);
		}
	}
	
	public void validateElementNotExist(AppiumDriver driver, By by) throws IOException, ConfigurationException {
	//	try {
		//	WebDriverWait wait = new WebDriverWait(driver, 5);
			//WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(by));
		//} catch (Exception e) {
			//Reporter.addStepLog(e.getMessage());
	//	}
		if (!isElementExist(driver, by)) {
			Assert.assertTrue(true);
		} else {
			//driver.quit();
			Calendar cal = Calendar.getInstance();	
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
			String imageName =dateFormat.format(cal.getTime());
			TakeScreenShot(driver, imageName);
			PropertiesConfiguration config = new PropertiesConfiguration("config.properties");
			String relPath =(String) config.getProperty("Relpath");
			Reporter.addScreenCaptureFromPath(relPath, "");
			Assert.assertTrue(false);
		}
	}
	public void validateElementBGcolor(AppiumDriver driver, By by, String color) throws IOException, ConfigurationException {

			WebDriverWait wait = new WebDriverWait(driver, 5);
			String colorBackGround =getCssValue(driver, by, "background-color");
			System.out.println(colorBackGround);
		if (colorBackGround.contains(color)) {
			Assert.assertTrue(true);
		} else {
			Calendar cal = Calendar.getInstance();	
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
			String imageName =dateFormat.format(cal.getTime());
			TakeScreenShot(driver, imageName);
			PropertiesConfiguration config = new PropertiesConfiguration("config.properties");
			String relPath =(String) config.getProperty("Relpath");
			Reporter.addScreenCaptureFromPath(relPath, "");
			Assert.assertTrue(false);
		}
	}

	public void validateChildElementExist(AppiumDriver driver, WebElement element) throws ConfigurationException, IOException {
		WebElement element1=null;
		try {
			WebDriverWait wait = new WebDriverWait(driver, 5);
			element1 = wait.until(ExpectedConditions.visibilityOf(element));
		
		} catch (Exception e) {
			Reporter.addStepLog(e.getMessage());
		}
		if (element1.isDisplayed()) {
			Assert.assertTrue(true);
		} else {
			Calendar cal = Calendar.getInstance();	
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
			String imageName =dateFormat.format(cal.getTime());
			TakeScreenShot(driver, imageName);
			PropertiesConfiguration config = new PropertiesConfiguration("config.properties");
			String relPath =(String) config.getProperty("Relpath");
			Reporter.addScreenCaptureFromPath(relPath, "");
			Assert.assertTrue(false);
		}
	}
	
	void TakeScreenShot(AppiumDriver driver,String imageName) throws IOException, ConfigurationException{
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		PropertiesConfiguration config = new PropertiesConfiguration("config.properties");
		String timeStamp = (String) config.getProperty("timeStamp");
		String filepath = System.getProperty("user.dir")+"\\target\\"+timeStamp+"\\Screenshot\\"+imageName+".jpg";
		String Relpath = ".\\Screenshot\\"+imageName+".jpg";
		config.setProperty("Relpath", Relpath);
		config.save();
		FileUtils.copyFile(scrFile, new File(filepath));		
	}
	public void retryStockHero() {
	}

	public void findElementWithScroll(AppiumDriver driver, By by) {
		int count = 0;
		
		while (!isElementExist(driver, by) && count < 20) {
			scroll(driver, 0.7, 0.2);
			count++;
		}
	}
	public void findElementWithScrollToRight(AppiumDriver driver, By by) {
		int count = 0;
		
		while (!isElementExist(driver, by) && count < 20) {
			scrolltoRight(driver, 0.7, 0.2);
			count++;
		}
	}

	public void scroll(AppiumDriver driver, Double startY, Double endY) {
		Dimension size = driver.manage().window().getSize();
		//int startx=size.width/2;
		//int starty=(int)(size.height*0.9);
		//int endx=size.width/2;
		//int endy=(int)(size.height*0.2);
		driver.swipe(5,1800,5,-600, 100);
		
		}
	public void scrolltoRight(AppiumDriver driver, Double startY, Double endY) {
		driver.swipe(1086,552,732,552, 100);
		}

	public Boolean isElementVisible(AppiumDriver driver, By by) {
		try {
			return driver.findElement(by).isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
}
