@MT-56
Feature: Login Functionality of stockHERO app
@MT-43
Scenario Outline: Send the app to background
	
 		Given I start application by name "stockHERO" and navigate to the starting point
 		When I choose the "<country>" from the country picker
  	Then I should see the list of stores
  	When I select a shop with "<StoreID>"
  	Then map should be displayed with "<StoreName>" and "<StoreAddress>"
  	When I choose "Join" option to log in
  	Then I log in to app as Stock Room using "<MyName>"
    When I navigate to the "Search" button
    And  I navigate to search box and look for shoes' model "C77124"
    When I push the application to the background without logging off
    Then I reopen the application "stockHERO"
    Then Logout successfully
    And I close my application by name "stockHERO"
    
 Examples: 
    |StoreID	|country|city		|StoreName										  |StoreAddress							|MyName		  | LoginRole  |
    |3425	 	|Spain  |Zaragoza 	|adidas Shop-in-Shop Zaragoza, Paseo Independencia|Paseo Independencia,11,50001,Zaragoza|Test-1stFloor| SalesFloor | 
    
@MT-46
Scenario Outline: Check if Cancel comment works 
 		Given I start application by name "stockHERO" and navigate to the starting point
 		When I choose the "<country>" from the country picker
  	Then I should see the list of stores
  	When I select a shop with "<StoreID>"
  	Then map should be displayed with "<StoreName>" and "<StoreAddress>"
  	When I choose "Join" option to log in
  	Then I log in to app as Stock Room using "<MyName>"
    When I navigate to the "Search" button
    And  I navigate to search box and look for shoes' model "C77124"
    When I am on the chosen shoe site I select shoe size "5"
    And I click on "Comment" button and enter comment
    Then I cancel the comment
    And I close my application by name "stockHERO"
 
 Examples: 
    |StoreID	|country|city		|StoreName										  |StoreAddress							|MyName		  | LoginRole  |
    |3425	 	|Spain  |Zaragoza 	|adidas Shop-in-Shop Zaragoza, Paseo Independencia|Paseo Independencia,11,50001,Zaragoza|Test-1stFloor| SalesFloor |