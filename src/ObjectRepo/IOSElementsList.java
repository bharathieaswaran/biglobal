package ObjectRepo;

import org.openqa.selenium.By;

public final class IOSElementsList {
	// General
	public static By btnCancel = By.xpath("//*[@label='CANCEL']");

	// Article
	public static By btnArticles = By.xpath("//*[@name='articles']");
	public static By tblEmptyList = By.xpath("//*[@label='Empty list']");
	public static By tblSearchResults = By.xpath("//UIATableView[contains(@value,'rows')]");
	public static By btnArticlesScreenBack = By.xpath("//UIANavigationBar[@name='Search']/*[@label='Back']");
	public static By txtArticleSearch = By.xpath("//*[@label='Search']");
	public static By btnArticleSearch = By.xpath("//UIAButton[@label='Search']");
	public static By txtSearch = By.xpath("//*[@name='Search Field']");
	public static By articleTable = By.xpath("//AppiumAUT/UIAApplication[1]/UIAWindow[1]/UIATableView[2]");
	public static By articleTableCell = By.className("UIATableCell");
	public static By btnArticlesNavigationBar = By.xpath("//*[@label='Articles']");
	public static By btnArticlesNavigationBar1 = By.xpath("//UIANavigationBar[@name='Search']/*[@label='Articles']");
	
	// Filter
	public static By btnTasks = By.xpath("//*[@name='tasks']");
	public static By btnfilter = By.xpath("//*[@label='filter button']");
	public static By filterWomen = By.xpath("//*[@label='Women']");
	public static By filterApply = By.xpath("//*[@label='APPLY']");
	
	// Menu
	public static By btnMenu = By.xpath("//*[@label='menuBars']");
	public static By btnHelp = By.xpath("//*[@value='Help']");
	public static By btnLogout = By.xpath("//*[@value='Logout']");
	public static By filterOptionWomen = By.xpath("//*[@label='Women']");
	public static By pickerWheel = By.xpath("//UIAPickerWheel");
	public static By btnReminder = By.xpath("//*[@label='Reminder']");
	
	// *[@label=\"Women\"]
	// NavigationBar
	public static By btnBackNavigationBar = By.xpath("//UIANavigationBar/*[@label='Back']");
	public static By btnSelectCountryNavigationBar = By.xpath("//UIANavigationBar/*[@label='Select your country']");
	public static By btnTasksNavigationBar = By.xpath("//UIANavigationBar//*[@label='Tasks']");
	
	// Login
	public static By btnJoin = By.xpath("//*[@label='Join']");
	public static By btnUserRoleSalesFloor = By.xpath("//UIATableView/UIATableCell[@name='Sales Floor']");
	public static By btnUserRoleStockRoom = By.xpath("//UIATableView/UIATableCell[@name='Stock Room']");
	public static By txtUserName = By.xpath("//*[@name='User Name']");
	public static By btnNext = By.xpath("//*[@label='NEXT']");
	public static By btnLogin = By.xpath("//*[@label='LOGIN']");
	public static By btnLoginCancel = By.xpath("//*[@label='CANCEL']");
	public static By btnLoginDone = By.xpath("//*[@label='Done']");
	public static By btnSalesFloor = By.xpath("//*[@label='Sales Floor']");
	
	//End of Repository
	public static By btnSelectMarket = By.xpath("//UIAApplication/UIAWindow[1]/UIAScrollView[1]/UIAScrollView[1]/UIAScrollView[2]/UIAElement[1]");
	public static By btnCountryJapan = By.xpath("//*[@label='Japan']");
	public static By btnCustomerDirect = By.xpath("//*[@label='Image3370. This is a Link. ']");
	public static By BIGlobalBICuonsumerDirect = By.xpath("//*[@label='Consumer Direct']");
	public static By GlobalBITest = By.xpath("//UIAStaticText[@label='Global BI TEST']");
	public static By GlobalBITestinstall = By.xpath("//UIAWebView/UIAButton[22]");
	public static By GlobalBITestinstallpopup = By.xpath("//UIAWebView/UIAButton[24]");
	public static By GlobalBITestUser = By.xpath("//UIATableCell[@name='Username']/UIATextField");
	public static By GlobalBITestPassword = By.xpath("//UIATableCell[@name='Password']/UIASecureTextField");
	public static By loginButton = By.xpath("//*[@label='Login']");
	public static By GlobalBIButton = By.xpath("//*[@label='GLOBAL BI']");
	public static By BIGlobalmobileDBFootwear = By.xpath("//*[@label='Image3675. This is a Link. ']");
	public static By BIGlobalmobileDBApparel = By.xpath("//*[@label='Image3677. This is a Link. ']");
	public static By BIGlobalmobileDBAG = By.xpath("//*[@label='Image3678. This is a Link. ']");
	public static By BIGlobalbacktoHome = By.xpath("//*[@label='Image3166. This is a Link. ']");
	public static By BIGlobalMobiledashboardApply = By.xpath("//*[@label='Apply']");
	public static By BIGlobalOK = By.xpath("//*[@label='OK']");
	public static By BIGlobalStoreBranding = By.xpath("//*[@label='Store Branding: ']");
	public static By BIGlobalStoreType = By.xpath("//*[@label='Store Type: ']");
	public static By BIGlobalclosedWD = By.xpath("//*[@label='# of closed WD YTD: ']");
	public static By BIGlobalStoreStatus = By.xpath("//*[@label='Store Status Group: ']");
	public static By BIGlobalCategories = By.xpath("//*[contains(@label,'Categories')]");
	public static By BIGlobalOverview = By.xpath("//*[contains(@label,'Overview')]");
	public static By BIGlobalTopSeller = By.xpath("//*[contains(@label,'Top-Seller')]");
	public static By BIGlobalBlackHole = By.xpath("//*[contains(@label,'Black Hole')]");
	public static By BIGlobalStorevisit = By.xpath("//*[contains(@label,'Store Visit')]");
	public static By BIGlobalStorereport = By.xpath("//*[@label='Store Reporting']");
	public static By BIGlobalStoreReportCountry = By.xpath("//*[@label='Country:']");
	public static By BIGlobalStoreReportCity = By.xpath("//*[@label='City:']");
	public static By BIGlobalChannel = By.xpath("//*[@label='selector, currently selected item is:Own Retail']");
	public static By BIGlobalSelectRetail = By.xpath("//*[@label='Own Retail']");
	public static By BIGlobalSelectReportCountryJapan = By.xpath("//*[@label='JAPAN']");
	public static By BIGlobalSelectStore = By.xpath("//*[contains(@label,'selector, currently selected item is:Store')]");
	public static By BIGlobalSelectGlobalStore = By.xpath("//*[@label='Store (Global)']");
	public static By BIGlobalSelectStoreCode = By.xpath("//*[@label='selector, currently selected item is:']");
	public static By BIGlobalSelectCode = By.xpath("//*[contains(@label,'17902')]");
	public static By BIGlobalStoreVisitReportBtn = By.xpath("//*[@label='Store Visit Report']");
	public static By BIGlobalTopSellers = By.xpath("//XCUIElementTypeStaticText[@label='Top-Seller Report']");
	public static By BIGlobalselectfilterUSA = By.xpath("//*[@label='USA Reebok']");
	public static By BIGlobalselectApparel = By.xpath("//*[@label='APPAREL']");
	public static By BIGlobalChangeDivtionbtn = By.xpath("//*[@label='Change Division']");
	public static By BIGlobalTop50Articles = By.xpath("//XCUIElementTypeStaticText[@label='Top 50 Best Selling Articles']");
	public static By BIGlobalArticleDetailsbtn = By.xpath("//*[contains(@label,'Display Article Details')]");
	public static By BIGlobalArticleImage = By.xpath("//*[contains(@label,'Image')]");
	public static By BIGlobalStoreStocktab = By.xpath("//*[contains(@label,'Store Stock')]");
	public static By BIGlobalStoreSalestab = By.xpath("//*[contains(@label,'Store Sales')]");
	public static By BIGlobalArticleAnalysis = By.xpath("//*[@label='Article_Analysis_Link. This is a Link. ']");
	public static By BIGlobalProductDivLabel = By.xpath("//*[@label='Product Division:']");
	public static By BIGlobalGenderLabel = By.xpath("//*[@label='Gender/Age:']");
	public static By BIGlobalCategoryLabel = By.xpath("//*[@label='Category:']");
	public static By BIGlobalBarcodeKeyboard = By.xpath("//*[@label='barcode keypad']");
	public static By BIGlobalEnter = By.xpath("//*[@label='Enter']");
	public static By BIGlobalArticleApply = By.xpath("//UIANavigationBar[@name='Article']//*[@label='Apply']");
	public static By BIGlobalStdMarginvalue = By.xpath("//*[@label='Std. Margin %']");
	public static By BIGlobalNSvalue = By.xpath("//*[contains(@label,'This is a grid, with 1 rows and 1 columnsNS vs. Y-1 (%)')]");
	public static By BIGlobalSMvalue = By.xpath("//*[contains(@label,'This is a grid, with 1 rows and 1 columnsSM% vs. Y-1 (%p)')]");
	public static By BIGlobalHomePage = By.xpath("//*[contains(@label,'This is a Link')]");
	public static By BIGlobalDrilltoStores = By.xpath("//*[@label='Drill to Stores ( Total )']");
	public static By BIGlobalWkStoreDetails = By.xpath("//*[contains(@label,'Weekly Store Details')]");
	public static By BIGlobalFocusOn = By.xpath("//*[contains(@label,'FOCUS ON')]");
	public static By BIGlobalTop5Cat = By.xpath("//*[@label='Top 5 Categories - STD information - ']");
	public static By BIGlobalShowAll = By.xpath("//*[@label='Show All']");
	public static By BIGlobalBlackHoleGender = By.xpath("//*[@label='Gender:']");
	public static By BIGlobalBlackHoleDivision = By.xpath("//*[@label='Division:']");
	public static By BIGlobalNrArticles = By.xpath("//*[@label='Nr. of Articles']");
	public static By BIGlobalBlackHoletitle = By.xpath("//*[@label='Black Hole (Retail Only)']");
	public static By BIGlobalRunArticleAnalysis = By.xpath("//*[@label='Run Article Analysis']");
	public static By btnSelectCountry = By.xpath("//UIAApplication/UIAWindow[1]/UIAScrollView[1]/UIAScrollView[1]/UIAScrollView[2]/UIAScrollView[2]/UIAElement[1]");
	public static By btnWeeklyview = By.xpath("//*[@label='WEEKLY DASHBOARDS']");
	public static By btnDistricts = By.xpath("//*[@label='DISTRICTS']");
	public static By BIGlobalStdMarginlabel = By.xpath("//*[@label='Std. Margin %']");
	public static By BIGlobalNetSaleslabel = By.xpath("//*[@label='Net Sales']");
	public static By BIGlobalNetSalesvalue = By.xpath("//*[contains(@label,'This is a grid, with 1 rows and 1 columnsNet Sales')]");
	
	public static void main(String[] args) {

	}

}
