package steps;

import com.perfecto.reportium.client.ReportiumClient;


import ObjectRepo.IOSElementsList;
import Utilities.IOSOperations;
import Utilities.SetupPerfecto;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberJava.DriverScript;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;


import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

public class GlobalBIStepsDefs {
	AppiumDriver driver;
	DesiredCapabilities capabilities;
	IOSOperations iosOpn;

		@Before
	 	public void before() throws IOException, ConfigurationException {
		String host = "adidas.perfectomobile.com";
	//	SetupPerfecto perfecto = new SetupPerfecto();
		//capabilities = perfecto.setupCapabilities(DriverScript.deviceName);
		DesiredCapabilities capabilities = new DesiredCapabilities("", "", Platform.ANY);
		//capabilities.setCapability("model", "iPhone-7");
		capabilities.setCapability("model",DriverScript.deviceName);
		capabilities.setCapability("autoLaunch", true);
		System.out.println("device Name = "+DriverScript.deviceName );
		//capabilities.setCapability("automationName", "XCUITest");
		capabilities.setCapability("automationName", "appium");
		//capabilities.setCapability("bundleId", "Global-BI-iPhone-Test.vpn");
		//capabilities.setCapability("bundleId", "Global-BI-iPhone-Dev.vpn");
		capabilities.setCapability("autoLaunch", true);
		capabilities.setCapability("user", "bharathi.easwaran@externals.adidas-group.com");
		capabilities.setCapability("password", "Adidas$2017");
		capabilities.setCapability("scriptName", "BIAutomation");
		PropertiesConfiguration config = new PropertiesConfiguration("config.properties");
		driver = DriverScript.driver;
		//driver = new IOSDriver(new URL("https://" + host + "/nexperience/perfectomobile/wd/hub"), capabilities);
		//driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
			//driver=new  IOSDriver(new URL("http://127.0.0.1:4722/wd/hub"),capabilities);
			//driver.context("NATIVE_APP");
			iosOpn = new IOSOperations();
	   }
	    @After
	    public void after() {
	    	driver.closeApp();
	        driver.quit();
	    }

	
	//"^I start application by name \"([^\"]*)\"$"
	@Given("^I Launch application by name1 \"([^\"]*)\"$")
	public void i_start_application_by_name(String name) throws Throwable {
		try{
	      
		}catch (Exception e){
		}
	}
	// Select Market
	@When("^I select the Market \"(.*?)\"$")
	public void selectMarket(String market) throws InterruptedException, ConfigurationException, IOException{
		String xpath = "//*[@label='"+ market +"']";
		checkProgressStatus();
		ReportiumClient reportiumClient;
		HashMap<String, Object> params1 = new HashMap<>();
		params1.clear();
		WebElement element1;
		WebElement element;
		WebDriverWait wait = new WebDriverWait(driver,50);
		By GlobalBi =By.xpath("//*[@label='GLOBAL']");
		iosOpn.validateElementExist(driver, GlobalBi);
		By btnConsumer = By.xpath("//*[@label='CONSUMER DIRECT']");
		iosOpn.findElementWithScrollToRight(driver, btnConsumer);
		iosOpn.clickOnElement(driver, btnConsumer);
		//iosOpn.clickOnElement(driver, IOSElementsList.btnCustomerDirect);
		
		//iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalBICuonsumerDirect);
		String xpathElmenet = "//*[contains(@label,'selector, currently selected item is:Total')]";
		List<WebElement>  elements = driver.findElements(By.xpath(xpathElmenet));
		elements.get(0).click();
		element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		element.click();
	}
	@Given("Select Consumer Direct")
	public void selectConsumerDirect() throws ConfigurationException, IOException{
		checkProgressStatus();
		iosOpn.clickOnElement(driver, IOSElementsList.btnCustomerDirect);
		checkProgressStatus();
	}
	
    @Then("^I start application by name \"(.*?)\"$")
    public static void startAppByName(String name){
    
 
    	
    }
	@Given("Setup Global BI from World of App")
	public void installForWorldofApp() throws InterruptedException, ConfigurationException, IOException{
		iosOpn.clickOnElement(driver, IOSElementsList.GlobalBITest);	
		iosOpn.clickOnElement(driver, IOSElementsList.GlobalBITestinstall);	
		iosOpn.clickOnElement(driver, IOSElementsList.GlobalBITestinstallpopup);	
		Thread.sleep(180000);
		
	}
	public void recordVA(){
		Map<String, Object> GenderBTN = new HashMap<>();
		GenderBTN.put("content", "Gender");
		GenderBTN.put("timeout", "20");
		GenderBTN.put("ignorecase", "case");
		driver.executeScript("mobile:checkpoint:text", GenderBTN);

		Map<String, Object> RankBTN = new HashMap<>();
		RankBTN.put("content", "Rank");
		RankBTN.put("timeout", "20");
		RankBTN.put("ignorecase", "case");
		driver.executeScript("mobile:checkpoint:text", RankBTN);
		Map<String, Object> CategoryBTN = new HashMap<>();
		CategoryBTN.put("content", "Category");
		CategoryBTN.put("timeout", "20");
		CategoryBTN.put("ignorecase", "case");
		driver.executeScript("mobile:checkpoint:text", CategoryBTN);
		
		Map<String, Object> DivisionBTN = new HashMap<>();
		DivisionBTN.put("content", "Division");
		DivisionBTN.put("timeout", "20");
		DivisionBTN.put("ignorecase", "case");
		driver.executeScript("mobile:checkpoint:text", DivisionBTN);
		
	}
	
  
	@Given("^Setup configure Global BI \"(.*?)\"$")
	public void SetpupGlobalBI(String userid) throws ConfigurationException, IOException {
		iosOpn.validateElementExist(driver, IOSElementsList.GlobalBITestUser);
		iosOpn.sendKeysOnElement(driver, IOSElementsList.GlobalBITestUser, userid);
		iosOpn.sendKeysOnElement(driver, IOSElementsList.GlobalBITestPassword, "NHcd68cokFL-AEG}");
		iosOpn.validateElementExist(driver, IOSElementsList.loginButton);
		iosOpn.validateElementExist(driver, IOSElementsList.GlobalBIButton);		
	}
	
	@Then("Verify LO Mobile Dashboard")
	public void VerifyDBpage() throws ConfigurationException, IOException{
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalmobileDBFootwear);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalmobileDBApparel);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalmobileDBAG);
	}
	
	@Then("Verify LO Factory Dashboard")
	public void VerifyfactoryDB() throws ConfigurationException, IOException{
		VerifyDBpage();
	}
	 
	@Then("Back to Home")
	public void backtoHome() throws ConfigurationException, IOException{
		
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalbacktoHome);	
	}
	
	@When("I select the Footwear")
	public void selectfootwear(String country) throws ConfigurationException, IOException{
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalmobileDBFootwear);
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalMobiledashboardApply);
	}
	
	@When("I select the Apparel")
	public void selectApparel(String country) throws ConfigurationException, IOException{
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalmobileDBApparel);
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalMobiledashboardApply);		
	}
	
	@When("I select the A&G")
	public void selectAG() throws ConfigurationException, IOException{
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalmobileDBAG);
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalMobiledashboardApply);
	}

	@When("^I select the Country \"(.*?)\"$")
	public void selectCountry(String country){
		String xpath = "//*[@label='"+ country +"']";
		String xpathElmenet = "//*[contains(@label,'selector, currently selected item is:Total')]";
		List<WebElement>  elements = driver.findElements(By.xpath(xpathElmenet));
		elements.get(0).click();
		WebDriverWait wait = new WebDriverWait(driver,50);
		WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		element.click();
	}
	
	
	@When("^I select the Currency \"(.*?)\"$")
	public void selectCurrency(String currency){
	//	WebElement element = driver.findElement("selector.currency." + currency);
		//element.click();
	}
	
	@When("I Click the WeeklyView")
	public void WeeklyView() throws ConfigurationException, IOException{
		checkProgressStatus();
		
		//driver.findElement(By.xpath("//*[@label='WEEKLY DASHBOARDS']")).click();
		iosOpn.clickOnElement(driver, IOSElementsList.btnWeeklyview);
		iosOpn.clickOnElement(driver, IOSElementsList.btnDistricts);
		
		checkProgressStatus();
		//VerifyObject(IOSElementsList.BIGlobalNetSaleslabel,IOSElementsList.btnWeeklyview);
		//BIGlobal.NetSales.label
	}
	@When("I Click the StoreVisit")
	public void StoreVisit() throws ConfigurationException, IOException{
		WebElement element;
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalBICuonsumerDirect);
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalStorevisit);
		VerifyObject(IOSElementsList.BIGlobalStorereport,IOSElementsList.BIGlobalStorevisit);
	}
	
	@Then("Verify StoreReportingPage")
	public void VerifyStoreReportingPage() throws ConfigurationException, IOException{
	
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalStorereport);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalStoreReportCountry);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalStoreReportCity);
	}
	
	
	@Then("Generate StoreVisitReport")
	public void StoreVisitReport() throws ConfigurationException, IOException{
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalChannel);
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalSelectRetail);
		//QAFelement = driver.findElement("StoreReportFilter");		
		//QAFelement.click();
		String xpathElmenet = "//*[@label='selector, currently selected item is:(All)']";
		List<WebElement>  elements = driver.findElements(By.xpath(xpathElmenet));
		elements.get(0).click();
		
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalSelectReportCountryJapan);
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalSelectStore);
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalSelectGlobalStore);
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalSelectStoreCode);
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalSelectCode);
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalStoreVisitReportBtn);

		//checkProgressStatus();
		VerifyObject(IOSElementsList.BIGlobalStoreBranding,IOSElementsList.BIGlobalStoreVisitReportBtn);
		verifystoredetails();
		SelectHomePage();
	//	QAFelement = driver.findElement("BIGlobal.HomePage");		
		//QAFelement.click();	
					
	}
	
	@When("I Click the TopSeller")
	public void ClickonTopSeller() throws ConfigurationException, IOException{
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalTopSellers);				
	}
	
	@Then("Select Country USA")
	public void SelectcountryUSA() throws ConfigurationException, IOException{
		iosOpn.validateElementExist(driver, IOSElementsList.btnSelectCountry);
		iosOpn.clickOnElement(driver, IOSElementsList.btnSelectCountry);	
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalselectfilterUSA);
	
					
	}
	
	
		
	@Then("Verify TopSellerFilterPage")
	public void VerifyTopSellerFilterPage() throws ConfigurationException, IOException{
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalselectApparel);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalChangeDivtionbtn);
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalChangeDivtionbtn);
		checkProgressStatus();
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalTop50Articles);
		VerifyObject(IOSElementsList.BIGlobalTop50Articles,IOSElementsList.BIGlobalChangeDivtionbtn);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalTop50Articles);

	}
	@Then("Generate ArticleAnalysis")
	public void GenerateArticleAnalysis() throws InterruptedException, ConfigurationException, IOException{
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalTop50Articles);
		String xpathElmenet = "//XCUIElementTypeOther[contains(@label,'Article:')]";
		List<WebElement>  elements = driver.findElements(By.xpath(xpathElmenet));
		System.out.println("lit of element " +elements.size());
		elements.get(0).click();
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalArticleDetailsbtn);
 
		//QAFelement = driver.findElement("BIGlobal.ArticleAnalysisTitle");	
		//retryexceptionandVerify(QAFelement);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalArticleImage);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalStoreStocktab);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalStoreSalestab);
		SelectHomePage();
		
	}
		
	@When("I Click the ArticleAnalysis")
	public void ArticleAnalysisClick() throws ConfigurationException, IOException{
		checkProgressStatus();
		iosOpn.clickOnElement(driver, IOSElementsList.btnCustomerDirect);
		checkProgressStatus();
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalArticleAnalysis);
		checkProgressStatus();
	}
	@Then("Verify ArticleInfo")
	public void verifyArticle() throws InterruptedException, ConfigurationException, IOException{

		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalArticleImage);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalProductDivLabel);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalGenderLabel);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalCategoryLabel);
		SelectHomePage();
		
	}
	
	@When("^I Enter valid BarCode \"(.*?)\"$")
	//@When("I Enter BarCode")
	public void EnterBarCode(String BarCodeno) throws ConfigurationException, IOException{
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalBarcodeKeyboard);
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalBarcodeKeyboard);
		//retryexceptionandClick(QAFelement);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalEnter);
		char substr ;
		int len = BarCodeno.length();
		for (int i=0; i< len; i++){
			try {
			substr = BarCodeno.charAt(i);
			System.out.println(BarCodeno+ "Substring" +substr);
			String xpath = "//XCUIElementTypeButton[@label='"+substr +"']";
			System.out.println(xpath);
			driver.findElement(By.xpath(xpath)).click();
			}catch(Exception e){System.out.println("error"+e.getMessage());}
			
		}
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalEnter);
		checkProgressStatus();
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalRunArticleAnalysis);
		checkProgressStatus();
		//retryexceptionandClick(QAFelement);

	}
	
	@When("^I Enter Invalid BarCode \"(.*?)\"$")
	//@When("I Enter BarCode")
	public void EnterInvalidBarCode(String BarCodeno) throws ConfigurationException, IOException{
		
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalBarcodeKeyboard);
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalBarcodeKeyboard);
		//retryexceptionandClick(QAFelement);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalEnter);
		char substr ;
		//String BarCodeno = "887383687673";
		int len = BarCodeno.length();
		for (int i=0; i< len; i++){
			try {
			substr = BarCodeno.charAt(i);
			System.out.println(BarCodeno+ "Substring" +substr);
			String xpath = "//XCUIElementTypeButton[@label='"+substr +"']";
			driver.findElement(By.xpath(xpath)).click();
			}catch(Exception e){System.out.println("error"+e.getMessage());}
			
		}
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalEnter);
		checkProgressStatus();
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalRunArticleAnalysis);
		checkProgressStatus();
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalArticleApply);
		checkProgressStatus();
	}

	@Then("Verify the WeeklyView")
	public void VerifyWeeklyview() throws ConfigurationException, IOException{
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalNetSaleslabel);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalStdMarginlabel);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalStdMarginvalue);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalNSvalue);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalSMvalue);
		DrilltoStores();
		//QAFelement = driver.findElement("BIGlobal.HomePage");
		//QAFelement.click();
		
		}

	public void SelectHomePage() throws ConfigurationException, IOException{

		if (iosOpn.isElementDisplayed(driver, IOSElementsList.BIGlobalHomePage)){
			iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalHomePage);
			System.out.println("Click on Home button");
		}		
	}
	public void DrilltoStores() throws ConfigurationException, IOException{

		String name =" BCS - JP";
		WebDriverWait wait = new WebDriverWait(driver,50);
		WebElement element ;
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalDrilltoStores);
		String xpathStoreName = "//*[contains(@label,'"+name+"')]";  
		//WebDriverWait wait = new WebDriverWait(driver,50);
		element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathStoreName)));
		element.click();	
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalWkStoreDetails);
		checkProgressStatus();
		//VerifyObject(IOSElementsList.BIGlobalStoreBranding,IOSElementsList.BIGlobalWkStoreDetails);
		verifystoredetails();
		verifyCategories();
		verifyBlackHole();
		SelectHomePage();
		
	}
	public void retryexceptionandVerify(WebElement QAFelement1){

		try {
			
		if (QAFelement1.isDisplayed()){
			QAFelement1.isDisplayed();
		}
		
		}catch (Exception e){
			try {
				WebElement QAFelement = driver.findElement(IOSElementsList.BIGlobalOK);		
				if (QAFelement.isDisplayed()){
					QAFelement.click();
					if (QAFelement1.isDisplayed()){
						QAFelement1.isDisplayed();
					}
				}
			}
			catch (Exception e1){ 
				
			}
			
		}
	}
	
	public void retryexceptionandClick(WebElement QAFelement1){
		
		WebElement QAFelement;
		try {
			
		if (QAFelement1.isDisplayed()){
			QAFelement1.click();
		}
		
		}catch (Exception e){
			try {
				QAFelement = driver.findElement(IOSElementsList.BIGlobalOK);		
				if (QAFelement.isDisplayed()){
					QAFelement.click();
					if (QAFelement1.isDisplayed()){
						QAFelement1.click();
					}
					
				}
			}
			catch (Exception e1){ 
				System.out.println(e.getMessage());
			}
			
		}
	}
	
	
	public void VerifyObject(By Object1, By Object2){
		
		WebElement element = driver.findElement(Object1);	
		try {
			
		if (element.isDisplayed()){
			iosOpn.validateElementExist(driver, Object1);
		}
		
		}catch (Exception e){
			try {
				element = driver.findElement(IOSElementsList.BIGlobalOK);		
				if (element.isDisplayed()){
					iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalOK);
					element = driver.findElement(Object2);	
					if (element.isDisplayed()){
						element.click();
					}
				}
			}
			catch (Exception e1){ 
				System.out.println(e.getMessage());
			}		
		}
	}
	
	//@Then("Verify the StoreDetails")
	public void verifystoredetails() throws ConfigurationException, IOException{
		
		WebElement element = driver.findElement(IOSElementsList.BIGlobalStoreBranding);		
		retryexceptionandVerify(element);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalStoreType);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalclosedWD);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalStoreStatus);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalCategories);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalOverview);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalTopSeller);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalBlackHole);
		
	}
	
	public void verifyCategories() throws ConfigurationException, IOException{
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalCategories);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalFocusOn);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalTop5Cat);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalShowAll);
		
	}
	public void verifyBlackHole() throws ConfigurationException, IOException{
		
		iosOpn.clickOnElement(driver, IOSElementsList.BIGlobalBlackHole);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalBlackHoleGender);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalBlackHoleDivision);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalNrArticles);
		iosOpn.validateElementExist(driver, IOSElementsList.BIGlobalBlackHoletitle);	
	}
	
	public void checkProgressStatus(){
	//	driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		WebElement element =null;
		try {
		do{
		element = driver.findElementByXPath("//*[@label='In progress']");
		System.out.println(element.isDisplayed());
		System.out.println("App processing data");
		}while (element.isDisplayed());
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		}catch(Exception e){} 
	}
	

  
	@Given("^Setupp \"(.*?)\"$")
	public void SetpupGlobalBI2(String userid) {

		WebElement element = driver.findElement(By.xpath("//UIATableCell[@name='Username']/UIATextField"));
		element.isDisplayed();
		element.sendKeys(userid);
		element = driver.findElement(By.xpath("//UIATableCell[@name='Password']/UIASecureTextField"));
		element.sendKeys("NHcd68cokFL-AEG}");
		element = driver.findElement(By.xpath("//*[@label='Login'])"));
		element.click();
		element = driver.findElement(By.xpath("//*[@label='GLOBAL BI']"));
		element.isDisplayed();
	
		
	}
}



