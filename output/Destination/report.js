$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("StockHero.feature");
formatter.feature({
  "line": 2,
  "name": "",
  "description": "",
  "id": "",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@MT-56"
    }
  ]
});
formatter.scenarioOutline({
  "comments": [
    {
      "line": 4,
      "value": "#@TEST_MT"
    }
  ],
  "line": 5,
  "name": "Check if Cancel comment works",
  "description": "",
  "id": ";check-if-cancel-comment-works",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 6,
  "name": "I start application by name \"stockHERO\" and navigate to the starting point",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "I choose the \"\u003ccountry\u003e\" from the country picker",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "I should see the list of stores",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "I select a shop with \"\u003cStoreID\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "map should be displayed with \"\u003cStoreName\u003e\" and \"\u003cStoreAddress\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "I choose \"Join\" option to log in",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "I log in to app as role \"\u003cLoginRole\u003e\" using \"\u003cMyName\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 13,
  "name": "I navigate to the \"Search\" button",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "I navigate to search box and look for shoes\u0027 model \"C77124\"",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I am on the chosen shoe site I select shoe size \"6\"",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "I click on \"Comment\" button and enter comment",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "I cancel the comment",
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I close my application by name \"stockHERO\"",
  "keyword": "And "
});
formatter.examples({
  "line": 20,
  "name": "",
  "description": "",
  "id": ";check-if-cancel-comment-works;",
  "rows": [
    {
      "cells": [
        "StoreID",
        "country",
        "city",
        "StoreName",
        "StoreAddress",
        "MyName",
        "LoginRole"
      ],
      "line": 21,
      "id": ";check-if-cancel-comment-works;;1"
    },
    {
      "cells": [
        "3425",
        "Spain",
        "Zaragoza",
        "adidas Shop-in-Shop Zaragoza, Paseo Independencia",
        "Paseo Independencia,11,50001,Zaragoza",
        "Test-1stFloor",
        "SalesFloor"
      ],
      "line": 22,
      "id": ";check-if-cancel-comment-works;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 11927758122,
  "status": "passed"
});
formatter.scenario({
  "line": 22,
  "name": "Check if Cancel comment works",
  "description": "",
  "id": ";check-if-cancel-comment-works;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@MT-56"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "I start application by name \"stockHERO\" and navigate to the starting point",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "I choose the \"Spain\" from the country picker",
  "matchedColumns": [
    1
  ],
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "I should see the list of stores",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "I select a shop with \"3425\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "map should be displayed with \"adidas Shop-in-Shop Zaragoza, Paseo Independencia\" and \"Paseo Independencia,11,50001,Zaragoza\"",
  "matchedColumns": [
    3,
    4
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "I choose \"Join\" option to log in",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "I log in to app as role \"SalesFloor\" using \"Test-1stFloor\"",
  "matchedColumns": [
    5,
    6
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 13,
  "name": "I navigate to the \"Search\" button",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "I navigate to search box and look for shoes\u0027 model \"C77124\"",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I am on the chosen shoe site I select shoe size \"6\"",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "I click on \"Comment\" button and enter comment",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "I cancel the comment",
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I close my application by name \"stockHERO\"",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "stockHERO",
      "offset": 29
    }
  ],
  "location": "LoginStepsDefs.startAppByNameWithStartingPoint(String)"
});
formatter.result({
  "duration": 24322982682,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Spain",
      "offset": 14
    }
  ],
  "location": "LoginStepsDefs.chooseCountryFromList(String)"
});
formatter.result({
  "duration": 34238645038,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepsDefs.seeListOfStores()"
});
formatter.result({
  "duration": 6518644217,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "3425",
      "offset": 22
    }
  ],
  "location": "LoginStepsDefs.chooseStoreID(String)"
});
formatter.result({
  "duration": 35156272465,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "adidas Shop-in-Shop Zaragoza, Paseo Independencia",
      "offset": 30
    },
    {
      "val": "Paseo Independencia,11,50001,Zaragoza",
      "offset": 86
    }
  ],
  "location": "LoginStepsDefs.findStoreNameAndAddress(String,String)"
});
formatter.result({
  "duration": 18685524639,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Join",
      "offset": 10
    }
  ],
  "location": "LoginStepsDefs.findJoinBtn(String)"
});
formatter.result({
  "duration": 8576783349,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "SalesFloor",
      "offset": 25
    },
    {
      "val": "Test-1stFloor",
      "offset": 44
    }
  ],
  "location": "LoginStepsDefs.logIntoStockRoom(String,String)"
});
formatter.result({
  "duration": 54235759657,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Search",
      "offset": 19
    }
  ],
  "location": "LoginStepsDefs.searchBtn(String)"
});
formatter.result({
  "duration": 7643769787,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "C77124",
      "offset": 52
    }
  ],
  "location": "LoginStepsDefs.goToSearchBox(String)"
});
formatter.result({
  "duration": 93562582244,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "6",
      "offset": 49
    }
  ],
  "location": "LoginStepsDefs.chooseShoeSize(String)"
});
formatter.result({
  "duration": 11070879800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comment",
      "offset": 12
    }
  ],
  "location": "LoginStepsDefs.cancelCmt(String)"
});
formatter.result({
  "duration": 14537390858,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepsDefs.cancelClk()"
});
formatter.result({
  "duration": 12659327840,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "stockHERO",
      "offset": 32
    }
  ],
  "location": "LoginStepsDefs.closeApp(String)"
});
formatter.result({
  "duration": 769515883,
  "status": "passed"
});
formatter.after({
  "duration": 13527372635,
  "status": "passed"
});
});